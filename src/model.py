import torch
import torch.nn as nn
import torch.nn.functional as F
import math

class CausalSelfAttention(nn.Module):
    """implements the self attention mechanism, ensuring that the prediction for a particular
    token can only be influenced by earlier tokens in the sequence."""
    def __init__(self, embed_size, num_heads):
        super(CausalSelfAttention, self).__init__()
        self.embed_size = embed_size
        self.num_heads = num_heads
        self.head_dim = embed_size // num_heads

        #Linear layers to transform the input into queries, keys, and values for the attention mechanism:
        self.W_q = nn.Linear(embed_size, embed_size) #query layer
        self.W_k = nn.Linear(embed_size, embed_size) #key layer
        self.W_v = nn.Linear(embed_size, embed_size)#value layer
        self.fc_out = nn.Linear(embed_size, embed_size) #final layer to be applied after computing the attention mechanism

    def forward(self, x, mask):
        """Computes the attention score, applies the mask
        and uses scores to weigh for the attention mechanism"""
        N, seq_length, embed_size = x.shape

        #Split the embedding into self.num_heads different pieces
        Q = self.W_q(x).view(N, seq_length, self.num_heads, self.head_dim)
        K = self.W_k(x).view(N, seq_length, self.num_heads, self.head_dim)
        V = self.W_v(x).view(N, seq_length, self.num_heads, self.head_dim)

        #Calculate the attention scores:
        energy = torch.einsum("nqhd,nkhd->nhqk", [Q, K])  # Query-Key dot product
        if mask is not None:
            energy = energy.masked_fill(mask == 0, float("-1e20"))
        attention = F.softmax(energy / (self.embed_size ** (1 / 2)), dim=3)

        #Applying the attention to the values
        out = torch.einsum("nhql,nlhd->nqhd", [attention, V]).reshape(
            N, seq_length, embed_size
        )

        #Final linear layer
        out = self.fc_out(out)
        return out

class TransformerBlock(nn.Module):
    def __init__(self, embed_size, num_heads, dropout):
        super(TransformerBlock, self).__init__()
        self.attention = CausalSelfAttention(embed_size, num_heads)
        self.norm1 = nn.LayerNorm(embed_size)
        self.norm2 = nn.LayerNorm(embed_size)

        self.feed_forward = nn.Sequential(
            nn.Linear(embed_size, 4 * embed_size),
            nn.ReLU(),
            nn.Linear(4 * embed_size, embed_size),
        )

        self.dropout = nn.Dropout(dropout)

    def forward(self, x, mask):
        attention = self.attention(self.norm1(x), mask)
        x = x + self.dropout(attention)
        out = self.feed_forward(self.norm2(x))
        out = x + self.dropout(out)
        return out


class TransformerModel(nn.Module):
    def __init__(self, vocab_size = 65, embed_size=768, num_heads=12, num_layers=16, dropout=0.15):
        super(TransformerModel, self).__init__()
        self.WTE = nn.Embedding(vocab_size, embed_size)
        self.WPE = nn.Embedding(1000, embed_size)  #Assuming a maximum sequence length of 1000 for positional embeddings
        self.dropout = nn.Dropout(dropout)
        self.layers = nn.ModuleList([TransformerBlock(embed_size, num_heads, dropout) for _ in range(num_layers)])
        self.Final_LayerNorm = nn.LayerNorm(embed_size)
        self.LM_Head = nn.Linear(embed_size, vocab_size)  #Language Model Head

    def forward(self, idx, pos):
        tok_emb = self.WTE(idx)  # Token embeddings
        pos_emb = self.WPE(pos)  # Position embeddings
        x = self.dropout(tok_emb + pos_emb)  # Summing token and position embeddings followed by dropout

        #attention mask for causal attention
        mask = torch.triu(torch.ones(idx.size(1), idx.size(1)), diagonal=1).bool()
        mask = mask.to(idx.device)

        for Block in self.layers:
            x = Block(x, mask)
        x = self.Final_LayerNorm(x)

        logits = self.LM_Head(x) #logits for each tok in the vocab
        return logits

    def generate(self, input_ids, max_length=50):
        self.eval()
        with torch.no_grad():
            for _ in range(max_length):
                #Generate positional encodings
                positions = torch.arange(0, input_ids.size(1)).unsqueeze(0).to(input_ids.device)

                #Get model predictions
                outputs = self(input_ids, positions)
                predictions = outputs[:, -1, :]

                #Get the next token
                next_token = torch.argmax(predictions, dim=-1).unsqueeze(0)

                #Append the next token to the input_ids
                input_ids = torch.cat((input_ids, next_token), dim=1)

            return input_ids[0]








