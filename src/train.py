import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
import time

from model import TransformerModel
from preprocess import CharDataset, load_dataset

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
num_epochs = 20 
#Loading the dataset
file_path = '../data/raw_data.txt'
text = load_dataset(file_path)
dataset = CharDataset(text, block_size=128)

#Initializing the model
vocab_size = dataset.get_vocab_size()
model = TransformerModel(vocab_size=vocab_size).to(device)

#Splitting the dataset proportion is 80-20
total_size = len(dataset)
train_size = int(0.8 * total_size)
val_size = total_size - train_size

train_dataset, val_dataset = torch.utils.data.random_split(dataset, [train_size, val_size])
train_loader = DataLoader(train_dataset, batch_size=128, shuffle=True)
val_loader = DataLoader(val_dataset, batch_size=128, shuffle=False)

#Defining loss and optimizer
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=0.001)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.1)


def calculate_accuracy(output, target):
    """function to calculate accuracy, computes the proportion of correctly predicted tokens in a batch."""
    predictions = output.argmax(dim=1, keepdim=True)
    correct_predictions = predictions.eq(target.view_as(predictions)).sum().item()
    total_predictions = target.numel()
    return correct_predictions / total_predictions


def train(epoch, log_interval=200):
    """training function"""
    model.train()
    start_time = time.time()
    total_loss, total_acc = 0, 0

    print(f"\n--- Training Epoch {epoch} ---")
    for batch_idx, (data, targets) in enumerate(train_loader):
        data, targets = data.to(device), targets.to(device)
        positions = torch.arange(0, data.size(1)).unsqueeze(0).repeat(data.size(0), 1).to(device)
        optimizer.zero_grad()
        output = model(data, positions)
        loss = criterion(output.view(-1, vocab_size), targets.view(-1))
        acc = calculate_accuracy(output.view(-1, vocab_size), targets.view(-1))
        total_loss += loss.item()
        total_acc += acc
        loss.backward()
        optimizer.step()

        if batch_idx % log_interval == 0:
            print(f'Batch {batch_idx:>4}/{len(train_loader)}\tLoss: {loss.item():.6f}\tAcc: {acc:.4f}')

    avg_loss = total_loss / len(train_loader)
    avg_acc = total_acc / len(train_loader)
    print(f"--- Epoch {epoch} Completed: Avg. Loss: {avg_loss:.4f}, Avg. Accuracy: {avg_acc:.4f}")
    print(f"--- Training Time for Epoch: {time.time() - start_time:.2f}s ---")



def validate(model, dataloader, criterion):
    """validation function"""
    model.eval()
    total_loss, total_acc = 0, 0
    start_time = time.time()

    print(f"\n--- Validating Epoch ---")
    with torch.no_grad():
        for data, targets in dataloader:
            data, targets = data.to(device), targets.to(device)
            positions = torch.arange(0, data.size(1)).unsqueeze(0).repeat(data.size(0), 1).to(device)
            outputs = model(data, positions)
            loss = criterion(outputs.view(-1, vocab_size), targets.view(-1))
            acc = calculate_accuracy(outputs.view(-1, vocab_size), targets.view(-1))
            total_loss += loss.item()
            total_acc += acc

    avg_loss = total_loss / len(dataloader)
    avg_acc = total_acc / len(dataloader)
    print(f"--- Validation Completed: Avg. Loss: {avg_loss:.4f}, Avg. Accuracy: {avg_acc:.4f}")
    print(f"--- Validation Time: {time.time() - start_time:.2f}s ---")
    return avg_loss, avg_acc

def save_model(model, filename):
    """Saves the model state to a file."""
    torch.save(model.state_dict(), filename)

#Training loop
num_epochs = 20

for epoch in range(1, num_epochs + 1):
    train(epoch)
    val_loss, val_acc = validate(model, val_loader, criterion)
    print(f"\n### Epoch {epoch} Summary: Validation Loss: {val_loss:.4f}, Validation Accuracy: {val_acc:.4f} ###\n")
    scheduler.step()


# Saving the final model
save_model(model, "final_model_state_v5.pth")

