import torch
from model import TransformerModel
from preprocess import CharDataset, load_dataset

# Load the dataset
file_path = '../raw_data.txt'  # Update this path if needed
shakespeare_text = load_dataset(file_path)
dataset = CharDataset(shakespeare_text, block_size=128)

vocab_size = dataset.get_vocab_size()  # Get the vocabulary size
device = torch.device('cpu')  # Use CPU for inference

def tokenize(context):
    return torch.tensor([dataset.stoi[c] for c in context], dtype=torch.long).unsqueeze(0)

def tokens_to_string(tokens):
    return ''.join([dataset.itos[int(t)] for t in tokens])

# Load the saved file
checkpoint = torch.load('final_model.pth', map_location=device)

# Extract the state dictionary for the model
model_state_dict = checkpoint['model_state_dict']

# Load the state dictionary into the model
transformer_model = TransformerModel(vocab_size=vocab_size)
transformer_model.load_state_dict(model_state_dict)
transformer_model.eval()
transformer_model.to(device)

# Provide a seed string and generate text
seed_string = "God"
input_ids = tokenize(seed_string).to(device)

# Generate text
generated_ids = transformer_model.generate(input_ids, max_length=100)
generated_text = tokens_to_string(generated_ids)

print(generated_text)

