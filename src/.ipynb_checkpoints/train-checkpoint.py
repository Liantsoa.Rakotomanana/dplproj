import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
import os
import time

from model import TransformerModel
from preprocess import CharDataset, load_dataset

# Check if GPU is available and set the device accordingly
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Load the dataset
file_path = '../data/raw_data.txt'  # Update this path if needed
text = load_dataset(file_path)
dataset = CharDataset(text, block_size=128)

# Create the DataLoader
dataloader = DataLoader(dataset, batch_size=128, shuffle=True)

# Initialize the model
vocab_size = dataset.get_vocab_size()
model = TransformerModel(vocab_size=vocab_size).to(device)

# Loss and optimizer
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=0.001)

# Training function
def train(epoch, log_interval=200):
    model.train()
    start_time = time.time()

    for batch_idx, (data, targets) in enumerate(dataloader):
        # Send data to device
        data, targets = data.to(device), targets.to(device)

        # Generate positional indices
        positions = torch.arange(0, data.size(1)).unsqueeze(0).repeat(data.size(0), 1).to(device)

        # Forward pass
        optimizer.zero_grad()
        output = model(data, positions)
        loss = criterion(output.view(-1, vocab_size), targets.view(-1))

        # Backward pass and optimize
        loss.backward()
        optimizer.step()

        if batch_idx % log_interval == 0:
            print(f'Epoch: {epoch} [{batch_idx * len(data)}/{len(dataloader.dataset)} ({100. * batch_idx / len(dataloader):.0f}%)]\tLoss: {loss.item():.6f}')
    
    print(f"Training time for epoch: {time.time() - start_time:.2f}s")


# Training loop
num_epochs = 10  # Adjust as needed
for epoch in range(1, num_epochs + 1):
    train(epoch)

