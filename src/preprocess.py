import torch
from torch.utils.data import Dataset
import os

class CharDataset(Dataset):
    """class to create a custom dataset for our LM"""
    def __init__(self, text, block_size=128):
        """constructor, takes the desired text and an optional block_size,
        (here i kept 128 to mimick the results made by the teacher"""
        self.block_size = block_size
        self.text = text
        self.chars = sorted(list(set(text))) #list of unique characters
        self.stoi = { ch:i for i, ch in enumerate(self.chars) }#transform every char to indices
        self.itos = { i:ch for i, ch in enumerate(self.chars) }# Map indices to characters
        self.data = [self.stoi[ch] for ch in text] # Convert all the text to a sequence of indices

    """utility functions: """
    def get_vocab_size(self):
        #Returns the number of unique characters, representing the vocabulary size
        return len(self.chars)

    def __len__(self):
        #Returns the number of complete blocks in the dataset
        return len(self.data) // self.block_size

    def __getitem__(self, idx):
        """grab a chunk of (block_size + 1) characters from the data, at this point, data is already a list of numbers"""
        chunk = self.data[idx * self.block_size : (idx + 1) * self.block_size + 1]#take chars from position 'idx*128' to '(idx +1) * 128 +1'
        x = torch.tensor(chunk[:-1], dtype=torch.long) #first 'block_size' elements
        y = torch.tensor(chunk[1:], dtype=torch.long)#shifted version
        return x, y


def load_dataset(file_path):
    """function to load the dataset"""
    with open(file_path, 'r', encoding='utf-8') as file:
        text = file.read()
    return text

""" This little part is needed as i needed to construct the file path relative to the script location, i did not want to change my script"""
current_directory = os.path.dirname(os.path.realpath(__file__))
data_directory = os.path.join(current_directory, '../data')
file_path = os.path.join(data_directory, 'raw_data.txt')


#Loading the dataset
shakespeare_text = load_dataset(file_path)

#Creating an instance of CharDataset with the Shakespeare text
block_size = 64  #To match with the version provided in the instructions
dataset = CharDataset(shakespeare_text, block_size=block_size)

#Display some basic information about the dataset
vocab_size = dataset.get_vocab_size()
num_samples = len(dataset)


print(f"Vocabulary size: {vocab_size}")
print(f"Number of samples: {num_samples}")





